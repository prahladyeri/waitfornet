__author__ = "Prahlad Yeri"
__email__ = "prahladyeri@yahoo.com"
__copyright__ = "(c) 2019 Prahlad Yeri"

__version__ = "1.0.2"
__title__ = "waitfornet"
__description__ = "Module to check whether you are online or not"
__license__ = "MIT"
